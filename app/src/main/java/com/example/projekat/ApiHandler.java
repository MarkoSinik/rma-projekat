package com.example.projekat;

import android.content.Context;
import android.widget.Toast;

import com.example.projekat.Model.NewsApiResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiHandler {

    Context context;

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://newsapi.org/v2/")  //base url
            .addConverterFactory(GsonConverterFactory.create()) //Gson za konvertovanje
            .build();

    public ApiHandler(Context context) {
        this.context = context;
    }

    public void getArticles(final OnFetchDataListener listener, String category, String query){
        //pomocu retrofit-a se kreira implementacija za getNewsApi
        GetNewsApi getNewsApi = retrofit.create(GetNewsApi.class);
        // u call objekat se stavlja request
        Call<NewsApiResponse> call = getNewsApi.callArticles("us", category, query, context.getString(R.string.api_key));

        try{
            //posto ne zelim da se call izvrsava sinhorno na main thread-u, metoda enqueue ga izvrsava u drugom thread-u
            call.enqueue(new Callback<NewsApiResponse>() {
                @Override //ako se dobije odgovor od servera
                public void onResponse(Call<NewsApiResponse> call, Response<NewsApiResponse> response) {
                    if(!response.isSuccessful()){
                        //odgovor moze biti 404
                        Toast.makeText(context, "Error!", Toast.LENGTH_SHORT).show();
                    }

                    listener.onFetchData(response.body().getArticles(), response.message());
                    //odgovor se prosljedjuje OnFetchDataListener-u koji se poziva u main activity-u
                }

                @Override  //ako se desila greska u komunikaciji sa serverom
                public void onFailure(Call<NewsApiResponse> call, Throwable t) {
                    listener.onError("Request failed!");
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public interface GetNewsApi{
        @GET("top-headlines") // relative url
        Call<NewsApiResponse> callArticles(
                @Query("country") String country,
                @Query("category") String category,
                @Query("q") String query,
                @Query("apiKey") String apiKey
        );

    }
}
