package com.example.projekat;

import com.example.projekat.Model.Article;

import java.util.List;


public interface OnFetchDataListener {
    void onFetchData(List<Article> articles, String message);
    void onError(String message);

}
