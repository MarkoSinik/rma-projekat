package com.example.projekat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projekat.Model.Article;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SelectedArticleActivity extends AppCompatActivity {

    Article article;
    TextView details_title;
    ImageView details_img;
    TextView details_author;
    TextView details_time;
    TextView details_description;
    TextView details_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_article);

        details_title = findViewById(R.id.details_title);
        details_img = findViewById(R.id.details_img);
        details_author = findViewById(R.id.details_author);
        details_time = findViewById(R.id.details_time);
        details_description = findViewById(R.id.details_description);
        details_content = findViewById(R.id.details_content);

        article = (Article) getIntent().getSerializableExtra("data");

        showArticle();
    }



    public void showArticle(){
        details_title.setText(article.getTitle());
        details_author.setText(article.getAuthor());
        details_time.setText(article.getPublishedAt());
        details_description.setText(article.getDescription());
        details_content.setText(article.getContent());

        if(article.getUrlToImage() != null){
            Picasso.get().load(article.getUrlToImage()).into(details_img);
        }

    }
}
