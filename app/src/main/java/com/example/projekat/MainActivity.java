package com.example.projekat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.projekat.Model.Article;
import com.example.projekat.Model.NewsApiResponse;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerView;
    CustomAdapter adapter;

    Button btn_business;
    Button btn_entertainment;
    Button btn_general;
    Button btn_health;
    Button btn_science;
    Button btn_sports;
    Button btn_technology;

    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_business = findViewById(R.id.btn_business);
        btn_business.setOnClickListener(this::onClick);

        btn_entertainment = findViewById(R.id.btn_entertainment);
        btn_entertainment.setOnClickListener(this);

        btn_general = findViewById(R.id.btn_general);
        btn_general.setOnClickListener(this);

        btn_health = findViewById(R.id.btn_health);
        btn_health.setOnClickListener(this::onClick);

        btn_science = findViewById(R.id.btn_science);
        btn_science.setOnClickListener(this::onClick);

        btn_sports = findViewById(R.id.btn_sports);
        btn_sports.setOnClickListener(this::onClick);

        btn_technology = findViewById(R.id.btn_technology);
        btn_technology.setOnClickListener(this::onClick);


        searchView = findViewById(R.id.search_bar);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ApiHandler apiHandler = new ApiHandler(MainActivity.this);
                apiHandler.getArticles(listener, "general", query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        ApiHandler apiHandler = new ApiHandler(this);
        apiHandler.getArticles(listener, "general", null); //po default-u ce kategorija biti general, query je rijec po kojij se pretrazuje
    }


    private OnFetchDataListener listener = new OnFetchDataListener() {
        @Override
        public void onFetchData(List<Article> articles, String message) {
            //kada se dobiju clanci prosljedjuju se adapteru
            if(articles.isEmpty()){
                Toast.makeText(MainActivity.this, "No results found", Toast.LENGTH_SHORT);
            }
            else{
                showNews(articles);
            }
        }

        @Override
        public void onError(String message) {
            Toast.makeText(MainActivity.this, "An error occured!", Toast.LENGTH_SHORT);
        }
    };

    private SelectArticleListener selectArticleListener = new SelectArticleListener() {
        @Override
        public void onArticleClicked(Article article) {
            startActivity(new Intent(MainActivity.this, SelectedArticleActivity.class)
                    .putExtra("data", article));
        }
    };


    public void showNews(List<Article> articles) {
        recyclerView = findViewById(R.id.recyclerMain);
        recyclerView.setHasFixedSize(true);
        //recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomAdapter(this, articles, selectArticleListener);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onClick(View view) {
        Button button = (Button) view;
        String category = button.getText().toString();

        ApiHandler apiHandler = new ApiHandler(this);
        apiHandler.getArticles(listener, category, null);
    }
}
