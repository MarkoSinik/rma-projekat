package com.example.projekat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projekat.Model.Article;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomViewHolder> {

    private Context context;
    private List<Article> articles;
    private SelectArticleListener selectArticleListener;

    public CustomAdapter(Context context, List<Article> articlesData, SelectArticleListener selectArticleListener) {
        this.context = context;
        this.articles = articlesData;
        this.selectArticleListener = selectArticleListener;
    }

    @NonNull
    @Override
    //kreiranje novog view-a
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CustomViewHolder(LayoutInflater.from(context).inflate(R.layout.headline_list_items, parent, false));
        // pomocu LayoutInflater-a se kreira view za jedan red koji ce biti ubacen u activity_main
    }

    @Override
    //vezivanje podataka za view holder
    public void onBindViewHolder(@NonNull CustomViewHolder holder, int position) {
        //position je indeks objekta

        //dobavljanje title-a i source-a i setovanje u view
        holder.article_title.setText(articles.get(position).getTitle());
        holder.article_source.setText(articles.get(position).getSource().getName());

        if(articles.get(position).getUrlToImage() != null){
            Picasso.get().load(articles.get(position).getUrlToImage()).into(holder.article_img);
            //ako postoji slika u api odgovoru postavlja se u article_img, u suprotnom se prikazuje not_avilable.jpg
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectArticleListener.onArticleClicked(articles.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }
}
