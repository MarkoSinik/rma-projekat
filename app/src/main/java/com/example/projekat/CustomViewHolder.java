package com.example.projekat;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class CustomViewHolder extends RecyclerView.ViewHolder {

    // ViewHolder je wrapper oko view-a koji je namjenjen da se koristi u RecyclerView
    // Definisan je za headline_list_items.xml

    TextView article_title;
    TextView article_source;
    ImageView article_img;
    CardView cardView;

    public CustomViewHolder(@NonNull View itemView) {
        super(itemView);
        article_title = itemView.findViewById(R.id.article_title);
        article_source = itemView.findViewById(R.id.article_source);
        article_img = itemView.findViewById(R.id.article_img);
        cardView = itemView.findViewById(R.id.main_container);
    }
}
