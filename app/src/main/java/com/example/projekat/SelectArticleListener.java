package com.example.projekat;

import com.example.projekat.Model.Article;

public interface SelectArticleListener {
    void onArticleClicked(Article article);
}
