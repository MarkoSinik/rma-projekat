# News App
Android application for browsing news articles and
categories. Data is fetched from https://newsapi.org/ .



## Description
This project was a part of my final exam for
Mobile App Development subject. Throughout this project,
I have had the opportunity to get familiar with **_Android_** app development in **_Java_** and learn about concepts such as Activities, Intents, Recycler view, Rest API, JSON and
others.


### Functionalities: 
- Find articles based on category
- Search by keyword
- Browse through the list of articles
- Read the article


### Preview

<img title="" alt="image" witdth="500" height="500" src="/uploads/6daf050f612046088878762d9a158a85/new_app_demo1.jpg"> 

<img title="" alt="image" witdth="500" height="500" src="/uploads/af5a5b16cd8d96654a4d0876b3126a50/new_app_demo2.jpg"> 

<img title="" alt="image" witdth="500" height="500" src="/uploads/1ff0ac79d4658c1a0cb9bd4ceaaeb919/new_app_demo3.jpg"> 

<img title="" alt="image" witdth="500" height="500" src="/uploads/64d0d728fb00427b8861da7f707398c1/new_app_demo4.jpg"> 




